# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :run_item do
    association :run, factory: :run
    association :device, factory: :device
    status "Not Started"
  end
end
