# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :device do
    name "Test Device"
    ip "10.2.3.12"
    os "ios5"
    device_type "ipad"
  end
end
