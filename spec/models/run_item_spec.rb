require 'spec_helper'

describe RunItem do
  before(:each) do

    @attr = FactoryGirl.attributes_for(:run_item)
  end

  it "should create a new instance given a valid attribute" do
    @run =  FactoryGirl.create(:run)
    @device =  FactoryGirl.create(:device)
    @attr[:run_id]=@run.id
    @attr[:device_id]= @device.id
    RunItem.create!(@attr)
  end

  it "should give default status as 'Not Started'" do
    @attr.delete(:status)
    run_item = RunItem.new(@attr)
    run_item.status.should == "Not Started"
  end

  it "should respond to complete? method" do
    run_item = FactoryGirl.create(:run_item)
    run_item.should respond_to :completed?
    run_item.should_not be_completed
  end

end
