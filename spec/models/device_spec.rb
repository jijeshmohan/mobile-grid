require 'spec_helper'

describe Device do
  before(:each) do
    @attr = FactoryGirl.attributes_for(:device)
  end

  it "should create a new instance given a valid attribute" do
    Device.create!(@attr)
  end

  it "should create an active device default" do
    d=Device.create!(@attr)
    d.should be_active
  end

  it "should disable a device" do
    d=Device.create!(@attr)
    d.should be_active
    d.disable
    d.should_not be_active
  end

  it "should enable a device" do
    d=Device.create!(@attr.merge(:active => false))
    d.should_not be_active
    d.enable
    d.should be_active
  end

  describe "name" do
    it "should be present" do
      no_name_device = Device.new(@attr.merge(:name => ""))
      no_name_device.should_not be_valid
    end
    it "should be unique" do
      FactoryGirl.create(:device)
      same_name_device = Device.new(@attr)
      same_name_device.should_not be_valid
    end
  end

  describe "ip" do
    it "should be present" do
      no_ip_device = Device.new(@attr.merge(:ip => ""))
      no_ip_device.should_not be_valid
    end
    it "should be unique" do
      FactoryGirl.create(:device)
      same_ip_device = Device.new(@attr)
      same_ip_device.should_not be_valid
    end
    it "should be valid" do
      device = Device.new(@attr.merge(:ip => "sample"))
      device.should_not be_valid
    end
  end

  describe "os" do
    it "should be present" do
      device = Device.new(@attr.merge(:os=> ""))
      device.should_not be_valid
    end
    it "should not accept other values" do
      device = Device.new(@attr.merge(:os=> "random"))
      device.should_not be_valid
      device.os = "ios6"
      device.should be_valid
    end
  end

  describe "type" do
    it "should be present" do
      device = Device.new(@attr.merge(:device_type=> ""))
      device.should_not be_valid
    end
    it "should not accept other values" do
      device = Device.new(@attr.merge(:device_type=> "random"))
      device.should_not be_valid
      device.device_type = "ipad"
      device.should be_valid
    end
  end
end
