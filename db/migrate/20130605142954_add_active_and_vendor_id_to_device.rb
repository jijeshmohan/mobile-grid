class AddActiveAndVendorIdToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :active, :boolean
    add_column :devices, :vendor_id, :string
  end
end
