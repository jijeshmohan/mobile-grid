class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.string :name
      t.references :run_item

      t.timestamps
    end
    add_index :features, :run_item_id
  end
end
