class CreateScenarios < ActiveRecord::Migration
  def change
    create_table :scenarios do |t|
      t.string :name
      t.string :status
      t.references :feature

      t.timestamps
    end
    add_index :scenarios, :feature_id
  end
end
