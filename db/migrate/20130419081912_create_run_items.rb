class CreateRunItems < ActiveRecord::Migration
  def change
    create_table :run_items do |t|
      t.references :run
      t.references :device
      t.string :status

      t.timestamps
    end
    add_index :run_items, :run_id
    add_index :run_items, :device_id
  end
end
