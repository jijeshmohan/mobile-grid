MobileGrid::Application.routes.draw do

  resources :runs do
    member do
      get "report"
    end
    resources :run_details,:path => "/results", :only => :show
  end


  resources :devices do
     collection do
      post "heartbeat"
     end
  end

  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"

  devise_for :users

  resources :users

  match 'settings' => 'settings#show', :via => :get
  match 'settings/edit' => 'settings#edit', :via => :get
  match 'settings/update' => 'settings#update', :via => :post

end
