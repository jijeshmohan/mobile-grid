class CalabashJob < Struct.new(:run_item_id,:ip,:os,:device_type)
  def perform
    envs = {"NO_LAUNCH"=>"1","DEVICE"=>device_type,"OS"=>os,"DEVICE_ENDPOINT"=>"http://#{ip}:37265"}
    create_report_dir
    sleep rand(5)+1
    path = get_path
    result="-f json -o #{@path}/result.json -f html -o #{@path}/result.html SCREENSHOT_PATH=#{@path}/"
    system(envs,"cucumber #{path}/features #{result} #{ProjectSetting[:default_commands]}",:chdir=> path)
  end
  def before(job)
    update_status("Running")
  end
  def after(job)
    statuses = find_status
    status = "Completed"
    status = "Passed" if statuses.all?{|x| x == "passed"}
    status = "Failed" if statuses.any?{|x| x == "failed"}
    update_status(status)
  end
  def update_status(status)
    @run_item ||= RunItem.find(run_item_id)
    @run_item.status = status
    @run_item.save
  end

  def find_status
    begin
    result=File.read("#{@path}/result.json")
    rescue
      return ["failed"]
    end
    parsed_json = ActiveSupport::JSON.decode(result)
    @run_item ||= RunItem.find(run_item_id)
    status=[]
    parsed_json.each do |feature|
      f = @run_item.features.build(:name => feature["name"])
      feature["elements"].each do |scenario|
        if scenario["keyword"] == "Scenario"
          s = get_status(scenario)
          status << s
          f.scenarios.build({:name => scenario["name"], :status => s})
        end
      end
      f.save
    end
    status
  end

  def get_status(scenario)
    status = scenario["steps"].map do |step|
              step["result"]["status"]
            end
    return "passed" if status.all? {|s| s == "passed"}
    return "failed" if status.any? {|s| s == "failed"}
    return "skipped"
  end

  def create_report_dir
     p = Rails.root.join('public', 'reports', "#{run_item_id}")
     puts p
     @path = FileUtils.mkdir_p(p)[0]
  end

  def get_path
   return ProjectSetting[:project_path] if ProjectSetting[:git_url].strip.empty?
   return Rails.root.join('repo','gitrepo').to_path
  end
end
