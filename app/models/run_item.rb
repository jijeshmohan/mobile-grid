class RunItem < ActiveRecord::Base
  include ApplicationHelper

  belongs_to :run
  belongs_to :device
  attr_accessible :status,:device_id,:run_id
  accepts_nested_attributes_for :device
  belongs_to :run
  belongs_to :device

  has_many :features
  has_many :scenarios, :through => :features


 after_initialize :init

  def init
   self.status||= "Not Started"
  end

  def completed?
    !(self.status == "Not Started" || self.status == "Running")
  end

  after_create do |run_item|
    unless git?
      Delayed::Job.enqueue CalabashJob.new(run_item.id,run_item.device.ip,run_item.device.os,run_item.device.device_type)
    end
  end
end
