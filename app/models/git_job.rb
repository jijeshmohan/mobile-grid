class GitJob  < Struct.new(:run_id)
  def perform
    g= fetch_repo
    g.reset_hard
    g.pull
  end

  def before(job)

  end

  def after(job)

  end

  def success(job)
    @run ||= Run.find(run_id,:include => {:run_items => [:device]})
    @run.run_items.each do |item|
        Delayed::Job.enqueue CalabashJob.new(item.id,item.device.ip,item.device.os,item.device.device_type)
    end
  end

  def error(job, exception)

  end

  def failure(job)
    @run ||= Run.find(run_id,:include => {:run_items => [:device]})
    @run.run_items.each do |item|
      item.status="Failed"
      item.save
    end
  end


  def max_attempts
    return 2
  end


    def fetch_repo
      @path = get_repo_dir
      begin
        g = Git.open(@path + "/gitrepo")
      rescue
         g = Git.clone(ProjectSetting[:git_url], "gitrepo", :path => @path)
      end
    end

    def get_repo_dir
      Rails.root.join('repo').to_path
    end
end
