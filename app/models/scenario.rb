class Scenario < ActiveRecord::Base
  belongs_to :feature
  attr_accessible :name, :status
  scope :passed, where(:status => 'passed')
  scope :failed, where(:status => 'failed')
  scope :skipped, where(:status => 'skipped')
end
