class Run < ActiveRecord::Base
  include ApplicationHelper

  default_scope order('id desc')
  attr_accessible :start_date,:run_items_attributes
  has_many :run_items
  has_many :devices, :through => :run_items
  after_initialize :init

  accepts_nested_attributes_for :run_items,:reject_if => :all_blank, :allow_destroy => true

  def name
    "Run - #{self.id}"
  end

  def init
   self.start_date ||= Time.new
  end

  def completed?
    self.run_items.all?{|item| item.completed? }
  end

  def status
   return "Running" if self.run_items.any?{|item| item.status=="Running"}
   return "Failed" if self.run_items.any?{|item| item.status=="Failed"}
   return "Passed" if self.run_items.all?{|item| item.status=="Passed"}
   return "Completed" if self.run_items.all?{|item| item.status=="Completed" || item.status=="Skipped"}
   return "Not Started"
  end

  after_create do |run|
    if git?
      Delayed::Job.enqueue GitJob.new(run.id)
    end
  end
end
