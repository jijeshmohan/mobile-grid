class Feature < ActiveRecord::Base
  belongs_to :run_item
  attr_accessible :name

  has_many :scenarios
end
