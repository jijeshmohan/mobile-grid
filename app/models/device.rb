require 'resolv'

class Device < ActiveRecord::Base
  attr_accessible :ip, :name, :os, :device_type, :active,:vendor_id,:model
  scope :active, where(active: true)

  validates :name, :presence => true, :uniqueness => {:scope => :active}

  validates :ip, :presence => true, :uniqueness => {:scope => :active},
  :format => { :with => Resolv::IPv4::Regex }

  validates :os, :presence => true, :inclusion => { :in => %w(ios5 ios6),
    :message => "%{value} is not a valid os" }

  validates :device_type, :presence => true, :inclusion => { :in => %w(ipad iphone),
    :message => "%{value} is not a valid device type" }


  has_many :run_items
  has_many :runs, :through => :run_items


  after_initialize :init

  def init
   self.active = true if self.active.nil?
  end


  def enable
    self.active=true
    save!
  end

  def disable
    self.active=false
    save!
  end
end
