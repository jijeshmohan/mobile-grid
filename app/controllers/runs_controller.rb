class RunsController < ApplicationController
  # GET /runs
  # GET /runs.json
  def index
    @runs = Run.page(params[:page]).per(10).includes(:run_items)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @runs }
    end
  end

  def report
     @run = Run.find(params[:id],:include => {:run_items => [:device, {:features => :scenarios}]})
     unless @run.completed?
      redirect_to @run, notice: 'Run is not completed..'
      return
     end
  end
  # GET /runs/1
  # GET /runs/1.json
  def show
    @run = Run.find(params[:id],:include => {:run_items => [:device, :scenarios]})

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @run }
      format.js
    end
  end

  # GET /runs/new
  # GET /runs/new.json
  def new
    @run = Run.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @run }
    end
  end


  # POST /runs
  # POST /runs.json
  def create
    devices = params[:run][:device_ids]
    params[:run].delete(:device_ids)
    @run = Run.new(params[:run])

    unless devices.any? {|d| not d.to_s.empty?}
      respond_to do |format|
        format.html {
          flash[:error]='There are no devices selected.'
          redirect_to action: "new"
        }
        format.json { render json: @run.errors, status: :unprocessable_entity }
      end
      return
    end

    devices.each do |d_id|
      @run.run_items << RunItem.new(:device_id => d_id) unless d_id.empty?
    end

    @run.run_items
    respond_to do |format|
      if @run.save
        format.html { redirect_to @run}
        format.json { render json: @run, status: :created, location: @run }
      else
        format.html { render action: "new" }
        format.json { render json: @run.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /runs/1
  # DELETE /runs/1.json
  def destroy
    @run = Run.find(params[:id])
    @run.destroy

    respond_to do |format|
      format.html { redirect_to runs_url }
      format.json { head :no_content }
    end
  end
end
