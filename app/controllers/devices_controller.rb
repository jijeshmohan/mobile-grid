class DevicesController < ApplicationController
  # GET /devices
  # GET /devices.json
  skip_before_filter :verify_authenticity_token, :only => [:heartbeat]

  def index
    @devices = Device.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @devices }
    end
  end

  # GET /devices/1
  # GET /devices/1.json
  def show
    @device = Device.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @device }
    end
  end

  def heartbeat
    msg = ActiveSupport::JSON.decode(params[:msg])
    msg=msg.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    msg[:os] = "ios"+msg[:os][0]
    msg[:device_type] = msg[:model].downcase.split(" ")[0]
    msg[:active]=true
    msg.delete(:model)

    disable_devices(msg[:ip],msg[:name])

    device = Device.find_by_vendor_id(msg[:vendor_id])
    if device.nil?
      result = Device.create(msg)
    else
      result = device.update_attributes(msg)
    end

    render :text => "#{result}"
  end

  # GET /devices/new
  # GET /devices/new.json
  def new
    @device = Device.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @device }
    end
  end

  # GET /devices/1/edit
  def edit
    @device = Device.find(params[:id])
  end

  # POST /devices
  # POST /devices.json
  def create
    @device = Device.new(params[:device])

    respond_to do |format|
      if @device.save
        format.html { redirect_to @device, notice: 'Device was successfully created.' }
        format.json { render json: @device, status: :created, location: @device }
      else
        format.html { render action: "new" }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /devices/1
  # PUT /devices/1.json
  def update
    @device = Device.find(params[:id])

    respond_to do |format|
      if @device.update_attributes(params[:device])
        format.html { redirect_to @device, notice: 'Device was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.json
  def destroy
    @device = Device.find(params[:id])
    @device.destroy

    respond_to do |format|
      format.html { redirect_to devices_url }
      format.json { head :no_content }
    end
  end

  private
  def disable_devices(ip,name)
      d=Device.find_by_ip(ip)
      d.disable unless d.nil?
      d= Device.find_by_name(name)
      d.disable unless d.nil?
  end
end
