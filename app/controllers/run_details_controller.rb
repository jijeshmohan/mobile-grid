class RunDetailsController < ApplicationController
   # GET /runs/1
  # GET /runs/1.json
  def show
    @run_item = RunItem.find(params[:id],:include=> [:device, {:features => :scenarios},:run])
    # @run = Run.find(params[:id],:include => {:run_items => [:device, {:features => :scenarios}]})

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @run_item }
    end
  end

end
