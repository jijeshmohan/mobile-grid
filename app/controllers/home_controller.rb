class HomeController < ApplicationController
  def index
   # @runs =  Run.limit(15).includes(:run_items => :device )
    @runs = Run.limit(5).includes(:run_items => [:device, :scenarios])
    @runs.reverse!
    #@data = @runs.map { |r| {y: r.name, count: r.run_items.length, id: r.id }}
    # @data = @runs.map { |r|
    #   d= {}
    #   d["y"] = r.name
    #   r.run_items.map { |i|
    #     d[i.device.name]=i.scenarios.select {|s| s.status == "passed"}.length
    #   }
    #   d[:id] = r.id
    #   d
    # }

   @devices = Device.all
   @device_data=device_graph_data(@devices)
  end

 # "device_name" => { "run" => "Run-11","id"=>"11", "passed"=>10,"failed"=>2,"total"=>22}

  private
  def device_graph_data(devices)
    devices.map do |device|
      dd={}
      dd[device.name]=  @runs.first(3).map { |r|
          d={}
          d["run"] = r.name
          d["id"] = r.id
          item = r.run_items.detect{|item| item.device.name == device.name }
          d.merge(calculate_counts(item))
       }
       dd
    end
  end
  def calculate_counts(item)
    result={"passed"=>0,"failed"=>0,"skipped"=>0,"total"=>0}
    return result if item.nil?
    result["passed"]=item.scenarios.select {|s| s.status == "passed"}.length
    result["failed"]=item.scenarios.select {|s| s.status == "failed"}.length
    result["skipped"]=item.scenarios.select {|s| s.status == "skipped"}.length
    result["total"]=item.scenarios.select {|s| s.status == "total"}.length
    result
  end
end
