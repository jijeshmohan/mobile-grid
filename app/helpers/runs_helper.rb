module RunsHelper
  def convert_status(status)
    stat=""
    if status.downcase == "passed"
     stat="icon-ok green"
    elsif status.downcase == "failed"
      stat="icon-remove red"
    else
      stat="icon-circle-blank blue"
    end

    content_tag(:i, "", :class=>stat)
  end

  def completed?(run)
    run.run_items.all? {|r| r.completed?}
  end

end
