module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def nav_link(link_text, link_path)
    class_name = current_page?(link_path) ? 'active' : ''

    content_tag(:li, :class => class_name) do
      link_to link_text, link_path
    end
  end

  def git?
    not ProjectSetting[:git_url].strip.empty?
  end

  def display_status(status)
    class_name = "badge"
    case status
      when "Completed"
        class_name += ' badge-info'
      when "Passed"
        class_name += ' badge-success'
      when "Running"
        class_name += ' badge-warning'
      when "Failed"
         class_name += ' badge-important'
    end
    content_tag(:span, :class => class_name) do
      status
    end
  end
end
