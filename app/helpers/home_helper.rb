module HomeHelper
  def data_for_donut(run_id, element_id)
  color_codes={failed: "#b94a48", passed: "#468847", skipped: "#3a87ad",total:"#9c9c9c"}

   data = Scenario.includes(:feature).where('features.run_item_id=?',run_id).group('scenarios.status').count
    items =[]
    colors = []
    data.each do |key,value|
      items << {label: key, value: value}
      colors << color_codes[key.to_sym]
    end

    javascript_tag "$(document).ready(function(){lastitems(#{items.to_json},#{colors.to_json},'#{element_id}',$);});"
  end

  def find_counts(run_item,status)
    run_item.scenarios.select {|s| s.status == status}.length
  end

  def data_for_bar(device_name, element_id)
     color_codes={failed: "#b94a48", passed: "#468847", skipped: "#3a87ad",total:"#9c9c9c"}
     data = @device_data.detect {|d| d.keys[0]==device_name}
     items = data[device_name].reverse
     javascript_tag "$(document).ready(function(){bar_chart(#{items.to_json},'#{element_id}',$);});"
  end
end
