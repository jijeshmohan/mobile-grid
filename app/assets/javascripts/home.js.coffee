# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

# jQuery ->
#   Morris.Donut
#     element: 'lastrunchart'
#     data: [
#       {label: "Download Sales", value: 12},
#       {label: "In-Store Sales", value: 30},
#       {label: "Mail-Order Sales", value: 20}
#     ]

$(document).ready ->
  window.lastrun = (data,devices,JQuery) ->
    Morris.Bar
      element: 'lastrunchart',
      data: data,
      xkey: 'y',
      ykeys: devices,
      labels: ['Devices']
      hoverCallback: (index, options, content) ->
        row = options.data[index];
        return "Run: " + row.y + "<br/><a href='/runs/" + row.id + "'>Detail</a>";

  window.lastitems = (data,color,element,JQuery) ->
    Morris.Donut
      element: element,
      data: data,
      colors: color

  window.bar_chart = (data,element,JQuery) ->
    Morris.Bar
      element: element,
      data: data,
      xkey: 'run',
      ykeys: ["passed","failed","skipped","total"],
      labels: ["Passed","Failed","Skipped","Total"]
      # hoverCallback: (index, options, content) ->
      #   row = options.data[index];
      #   return "Run: " + row.y + "<br/><a href='/runs/" + row.id + "'>Detail</a>";
  # window.lastrun = (data,JQuery) ->
  #   Morris.Donut
  #     element: 'lastrunchart'
  #     data: data
